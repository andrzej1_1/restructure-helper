use core::panic;
use std::path::PathBuf;
use std::fs;
use std::path::Component;

use anyhow::Result;

use crate::{file, markdown, path, link};

/// Combines 'root path', 'current file' and 'link' data to produce the absolute path.
fn relative_path_to_absolute(root: &PathBuf, file: &PathBuf, url: &str) -> PathBuf {
  // Obtain current directory.
  let mut current_file_dir = file.clone();
  current_file_dir.pop();

  // Examine URL components.
  let url_as_path = PathBuf::from(url);
  let url_components = url_as_path.components().into_iter().collect::<Vec<Component>>();

  // Case 1) simple component under current directory, e.g. "delegate-ui.md".
  if url_components.len() == 1 {
    let mut target = current_file_dir.clone();
    target.push(url_components.get(0).unwrap());

    if target.is_file() == true {
      return target;
    }
  }

  // Case 2) normal relative path.
  let mut target = current_file_dir.clone();
  for component in url_components.clone() {
    target.push(component);
  }
  target.set_extension("md");
  if target.is_file() == true {
    return target;
  }

  // Case 3) root relative path.
  let mut target = root.clone();
  for component in url_components.clone() {
    target.push(component);
  }
  target.set_extension("md");
  if target.is_file() == true {
    return target;
  }

  // Case 4) invalid parent specified, just skip single ".." and try relative path
  if url_components.get(0) == Some(&Component::ParentDir) {
    let mut target = current_file_dir.clone();
    for component in url_components.clone().iter().skip(1) {
      target.push(component);
    }
    target.set_extension("md");
    if target.is_file() == true {
      return target;
    }
  }

  panic!("Unable to determine absolute path for {}.", url);
}

pub fn run(root: &PathBuf) -> Result<()> {
  let files = file::find_files_recursive(root, "md")?;
  log::debug!("Found {} markdown files.", files.len());

  for file in &files {
    let content = fs::read_to_string(file)?;
    let mut new_content = content.clone();

    let markdown_links = markdown::find_links(&content)?;
    log::debug!("Found {} links in {:?}.", markdown_links.len(), file);
    for link in markdown_links {
      log::debug!("Working on {}.", link.full_match);
      // Skip absolute and external links.
      if link::is_absolute(&link.url) == true || link::is_external(&link.url) == true {
        log::debug!("Link {} is either absolute or external - skipping.", link.url);
        continue;
      }

      // Construct absolute link.
      let absolute_md_path = relative_path_to_absolute(root, file, &link.url);
      let docs_relative_path = path::build_relative_path(root, &absolute_md_path)?;
      let new_link = markdown::Link {
        title: link.title.clone(),
        url: format!("/{}", docs_relative_path.into_os_string().to_str().unwrap()),
        anchor: link.anchor.clone()
      };
      log::debug!("Absolute link constructed: {}.", new_link.to_string());

      // Update content.
      new_content = markdown::replace_link(&new_content, &link, &new_link);
    }

    fs::write(file, new_content).expect("Unable to write file");
    log::debug!("File content updated.");
  }

  Ok(())
}
