use anyhow::Result;

use std::path::{PathBuf, Component};
use std::fs;

use crate::{file, markdown, link};

pub fn run(root: &PathBuf) -> Result<()> {
  let files = file::find_files_recursive(root, "md")?;
  log::debug!("Found {} markdown files.", files.len());

  for file in &files {
    let content = fs::read_to_string(file)?;
    let mut new_content = content.clone();

    let markdown_links = markdown::find_links(&content)?;
    log::debug!("Found {} links in {:?}.", markdown_links.len(), file);
    for link in markdown_links {
      log::debug!("Working on {}.", link.full_match);

      // Skip non-absolute and external links.
      if link::is_absolute(&link.url) == false || link::is_external(&link.url) == true {
        log::debug!("Link {} is either non-absolute or external - skipping.", link.url);
        continue;
      }

      // Construct full absolute path from repo-related absolute URL.
      let mut absolute_path = root.clone();
      let url_as_path = PathBuf::from(&link.url);
      for component in url_as_path.components() {
        if component != Component::RootDir {
          absolute_path.push(component);
        }
      }

      // Skip files like .png, .jpg.
      match absolute_path.extension() {
        Some(ext) if ext != "md" => {
          // Static files are expected to be served from repo's /static, so we rebuild path with that folder injected.
          let mut static_asset_path = root.clone();
          static_asset_path.push("../../../static");
          for component in url_as_path.components() {
            if component != Component::RootDir {
              static_asset_path.push(component);
            }
          }
          // Make sure file exists.
          if static_asset_path.is_file() == false {
            log::warn!("Unable to locate static asset {:?}", static_asset_path);
          }
          log::debug!("Skipping static asset {:?}.", static_asset_path);
          continue;
        },
        _ => {},
      }

      // Skip known slug links.
      match absolute_path.extension() {
        None => {
          let known_slugs = ["/staking", "/staking/", "/dapp-dev-guide/build-on-casper/introduction",
            "/writing-contracts", "/tutorials/", "/counter/", "/counter-testnet", "/sdk", "/sdk/",
            "/workflow/developers" // this is not slug, but let us make it easier to work with...
          ];
          if known_slugs.contains(&link.url.as_str()) == true {
            log::warn!("Skipping known slug {} - rewrite not supported yet.", &link.url);
            continue;
          }
        },
        Some(_ext) => {},
      }

      // Otherwise file must be .md.
      absolute_path.set_extension("md");
      if absolute_path.is_file() == false {
        log::error!("Unable to locate {}", &link.url);
        continue;
      }

      // Find current directory.
      let mut current_dir = file.clone();
      current_dir.pop();

      // Compute relative path.
      let file_relative_path = pathdiff::diff_paths(absolute_path, current_dir.clone()).unwrap();
      let new_link = markdown::Link {
        title: link.title.clone(),
        url: format!("./{}", file_relative_path.clone().into_os_string().to_str().unwrap()),
        anchor: link.anchor.clone()
      };
      log::debug!("Relative link constructed: {}.", new_link.to_string());

      // [OPTIONAL!] Make sure only nested paths are allowed.
      let components = file_relative_path.components().into_iter().collect::<Vec<Component>>();
      if components.get(0) == Some(&Component::ParentDir) {
        log::debug!("Moving up the tree is disabled - skipping.");
        continue;
      }
      
      // [OPTIONAL!] Make sure no relative path is constructed for root located paths.
      if format!("{:?}", current_dir) == format!("{:?}", root) {
        log::debug!("Replacing root located links is disabled - skipping.");
        continue;
      }

      // Update content.
      new_content = markdown::replace_link(&new_content, &link, &new_link);
    }

    fs::write(file, new_content).expect("Unable to write file");
    log::debug!("File content updated.");
  }

  Ok(())
}
