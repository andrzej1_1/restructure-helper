use anyhow::Result;
use regex::Regex;

/// Structure holding Markdown link.
pub struct Link {
  pub title: String,
  pub url: String,
  pub anchor: Option<String>,
}

impl Link {
  pub fn to_string(&self) -> String {
    let anchor_part = self.anchor.as_ref().map(|x| format!("#{}", x)).unwrap_or(String::from(""));
    let link_str = format!("[{}]({}{})", self.title, self.url, anchor_part);

    link_str
  }
}

/// Structure holding Markdown link with full match.
pub struct LinkMatch {
  pub full_match: String,
  _link: Link,
}

impl std::ops::Deref for LinkMatch {
  type Target = Link;
  fn deref(&self) -> &Self::Target {
      &self._link
  }
}

/// Finds all Markdown links.
pub fn find_links(content: &str) -> Result<Vec<LinkMatch>>
{
  let mut links: Vec<LinkMatch> = vec![];
  let link_re = Regex::new(r"\[([^\]]+)\]\(([^)#]+)(?:#([^)]+))?\)")?;
  for capture in link_re.captures_iter(content) {
    let full_match = String::from(&capture[0]);
    let title = String::from(&capture[1]);
    let url = String::from(&capture[2]);
    let anchor = capture.get(3).map(|x| String::from(x.as_str()));
    links.push(LinkMatch { full_match, _link: Link { title, url, anchor}  })
  }

  Ok(links)
}

/// Replaces matched link with new one.
pub fn replace_link(content: &str, old: &LinkMatch, new: &Link) -> String
{
  let new_link_str = new.to_string();
  let new_content = content.replace(&old.full_match, &new_link_str);

  new_content
}