use std::process;

use anyhow::Result;

mod cli;
mod commands;
mod file;
mod link;
mod markdown;
mod path;

fn main() -> Result<()> {
  // Logger preparation.
  pretty_env_logger::init();

  // Parse and validate arguments.
  let args = cli::Args::parse_();
  if let Err(e) = args.validate() {
    log::error!("Invalid arguments: {}", e);
    process::exit(1);
  }

  // Handle command.
  let mode = args.mode;
  match mode {
    cli::Mode::AbsoluteLinks => commands::absolutize::run(&args.dir)?,
    cli::Mode::RelativeLinks => commands::relatize::run(&args.dir)?,
  };

  Ok(())
}
