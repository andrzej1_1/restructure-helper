use std::path::PathBuf;

use anyhow::{Result, Context};
use pathdiff::diff_paths;

/// Gets relative path.
/// E.g. "/foo/bar" + "/foo/bar/a/baz" = "a/baz".
pub fn build_relative_path(root: &PathBuf, child: &PathBuf) -> Result<PathBuf>
{
  let root_canonical = std::fs::canonicalize(root)?;
  let child_canonical = std::fs::canonicalize(child)?;
  let relative_path = diff_paths(&child_canonical, &root_canonical);

  relative_path.context("Unable to determine relative path.")
}