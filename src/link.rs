/// Check if link is external.
pub fn is_external(v: &str) -> bool {
  if v.starts_with("http://") || v.starts_with("https://") {
    return true;
  }

  false
}

/// Checks if link is absolute.
pub fn is_absolute(v: &str) -> bool {
  if v.starts_with("/") {
    return true;
  }

  false
}