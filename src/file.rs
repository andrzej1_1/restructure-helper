use std::ffi::OsStr;
use std::path::PathBuf;

use anyhow::Result;
use walkdir::WalkDir;

/// Gets list of all plain files with given extension under given directory.
pub fn find_files_recursive(root_dir: &PathBuf, extension: &str) -> Result<Vec<PathBuf>>
{
  // Walk through the whole tree.
  let mut files: Vec<PathBuf> = vec![];
  for entry in WalkDir::new(&root_dir) {
    let entry = entry?;
    let path = entry.path();

    // Skip non-files.
    if path.is_file() == false {
      continue;
    }

    // Skip files with non-.md extension.
    if path.extension() != Some(OsStr::new(extension)) {
      continue;
    }

    // Save path to file.
    files.push(path.to_path_buf());
  }

  Ok(files)
}
