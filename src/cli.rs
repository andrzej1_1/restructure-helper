use std::path::PathBuf;

use clap::{Parser, ValueHint};

#[derive(clap::ValueEnum, Debug, Clone)]
pub enum Mode {
  AbsoluteLinks,
  RelativeLinks,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
  /// Directory with Markdown files
  #[arg(short, long, value_hint = ValueHint::DirPath)]
  pub dir: PathBuf,

  #[arg(value_enum)]
  pub mode: Mode,
}

impl Args {
  pub fn parse_() -> Self {
    Args::parse()
  }

  pub fn validate(&self) -> Result<(), &str> {
    // Validate directory path.
    let dir = &self.dir;
    if dir.is_dir() == false {
      return Err("Invalid directory.");
    }

    Ok(())
  }
}
